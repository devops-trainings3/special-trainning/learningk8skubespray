## Nootes 

```yaml

helm create nginx-chart 
helm list 

helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
helm repo update 

# install chart to create a release 
helm install nginx-relase nginx-chart
helm uninstall nginx-relase

# in order to preview chart templates and stuff 
helm template nginx-chart 

helm upgrade  nginx-relase nginx-chart -f ./nginx-chart/prod-values.y
aml

# View history of the release with revision
helm history nginx-relase
# ROLLBACK 
helm rollback nginx-relase 1 



# Package the chart 
helm package nginx-chart 
helm package nginx-chart --version 0.1.1
helm package nginx-chart --version 0.1.2 --destination helm_storage
helm install nginx-staging nginx-chart-0.1.0.tgz
```



## Working with privates 

### Nexus OSS 
* Using the `curl` 
```yaml
curl -u admin:123456789123 https://nexus-repo.anuznomii.lol/repository/helm-hosted/ --upload-file nginx-chart-0.1.0.tgz
```

* Usingg the helm plugins 

```yaml
helm plugin install --version master https://github.com/sonatype-nexus-community/helm-nexus-push.git
helm nexus-push  -h


helm repo add my-repo https://nexus-repo.anuznomii.lol/repository/helm-hosted/
helm nexus-push my-repo nginx-chart-0.1.2.tgz --username admin --password 123456789123

helm pull my-repo/nginx-chart
helm pull my-repo/nginx-chart --version 0.1.0
helm install my-repo/nginx-chart --generate-name 
helm install nginx-release my-repo/nginx-chart
```
