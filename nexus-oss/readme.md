## Note 
We will first have to login to the nexus repo , in order push some images 

```yaml
docker login -u admin -p 123456789123 nexus-hub.anuznomii.lol 
docker login -u admin -p 123456789123 165.22.96.48:8085
docker pull nginx
docker tag nginx:latest nexus-hub.anuznomii.lol/nginx:1.0.0
docker push nexus-hub.anuznomii.lol/nginx:1.0.0
```
## To create secret for the private registry 
The quick and easy way to create a secret for dockerconfigjson is to use the following command 
```yaml
kubectl create secret docker-registry nexus-secret \
 --docker-server=nexus-hub.anuznomii.lol \
 --docker-username=admin \
 --docker-password=123456789123
```